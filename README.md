# README #

AzStudio Publicly available demo repository

### What is this repository for? ###

Holds the latest files capable of running the AzStudio demo.  It DOES NOT hold any database schema or data so it's useless to you unless you've contacted
Monza Cloud and arranged a hands-on demo.

### How do I get set up? ###

* Pull this repository
* Follow instructions in the "Intro to AzStudio" course.

### Contribution guidelines ###

Do not contribute to this repository.

### Who do I talk to? ###

* robert@monzacloud.com